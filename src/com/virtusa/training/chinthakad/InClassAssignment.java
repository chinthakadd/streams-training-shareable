package com.virtusa.training.chinthakad;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class InClassAssignment {

    public static void main(String[] args) {
        List<Person> persons = getPersons();
        System.out.println(persons.size());
        long currentTime = System.currentTimeMillis();
        List<Person> oldPeople = persons.parallelStream()
                .filter(InClassAssignment::getOlderPeople)
                .collect(Collectors.toList());
        long endTime = System.currentTimeMillis();
        System.out.println("Time Taken = " + (endTime - currentTime));
        System.out.println("Old Folks:" + oldPeople.size());

    }

    public static Boolean getOlderPeople(Person person) {
        // Check which thread the program runs.
        // System.out.println(Thread.currentThread().getName());
        try {
            Thread.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return person.age > 60;
    }

    public static List<Person> getPersons() {
        return IntStream.range(0, 10000)
                .mapToObj(value -> new Person("First" + value, "Last" + value,
                        new Random().nextInt(100))).collect(Collectors.toList());
    }
}

class Person {
    public Person(String firstname, String lastname, int age) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
    }

    String firstname;
    String lastname;
    int age;
}