package com.virtusa.training.chinthakad;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AssignmentAnswers {

    /**
     * Assignment 01:
     */
    private static double average(List<Integer> numbers) {
        return numbers.stream().mapToInt(Integer::intValue).average().orElse(0.0);
    }

    /**
     * Assignment 02:
     * Return a single List of String adding all the Strings from List of Lists given as the input.
     */
    public static List<String> getFlattenList(List<List<String>> listOfListOfString) {
        return Optional.ofNullable(listOfListOfString).orElse(new ArrayList<>())
                .stream().flatMap(Collection::stream).collect(Collectors.toList());
    }

    /**
     * Assignment 03:
     * This method prints Each key-value pair in a new line in the following format:
     * KEY: {key} = VALUE: {value}
     * <p>
     * Ex:
     * KEY: 1 = VALUE: JOEY
     * KEY: 2 = VALUE: CHANDLER
     */
    public static void printMapKeyValue(Map<String, String> map) {
        Optional.ofNullable(map).orElse(new HashMap<>())
                .entrySet()
                .stream()
                .map(entry -> String.format("KEY: %s = VALUE: %s", entry.getKey(), entry.getValue()))
                .forEach(System.out::println);
    }

    /**
     * Assignment 04:
     * Search the given for the key. If the key is found in the map, return the value. Otherwise return the String "NOT_FOUND".
     */
    public static String findValue(Map<String, String> map, String searchKey) {
        return Optional.ofNullable(map).orElse(new HashMap<>()).getOrDefault(searchKey, "NOT_FOUND");
    }

    /**
     * Assignment 05:
     * Search the given for the key. If the key is found in the map, return the value. Otherwise return the String "NOT_FOUND".
     */
    public static TreeMap<Integer, List<String>> convertListToMap(List<String> list) {
        return Optional.ofNullable(list).orElse(new ArrayList<>()).stream()
                .collect(Collectors.groupingBy(
                        String::length,
                        TreeMap::new,
                        Collectors.toList()));
    }


    /**
     * Assignment 06:
     */
    public static Map<Subject, Student> getBestScores(List<Student> students) {
        return Optional.ofNullable(students).orElse(new ArrayList<>())
                .stream()
                // Convert Student List to flatten List of Scores.
                .flatMap(
                        student -> student.report.entrySet()
                                .stream()
                                .map(scoreMap -> new Score(student, scoreMap.getKey(), scoreMap.getValue()))
                )
                // Group Scores by subject  -> Map<Subject, List<Score>>
                .collect(Collectors.groupingBy(score -> score.subject, Collectors.toList())).entrySet().stream()
                // Now Reduce List to a single student.
                .collect(Collectors.toMap(Map.Entry::getKey,
                        scoreMap -> scoreMap.getValue().stream()
                                .max(Comparator.comparing(o -> o.score))
                                // even though get() usage is bad, we are sure that student array is not empty by now.
                                .get().student));
    }

    /**
     * Assignment 07:
     */
    public void mergeFilesInUpperCase(String inputFolder, String outputFolder) {
        try {
            Files.walk(Paths.get(inputFolder))
                    .filter(Files::isRegularFile)
                    .flatMap((Function<Path, Stream<String>>) path -> {
                        try {
                            return Files.lines(path);
                        } catch (IOException e) {
                            return Stream.empty();
                        }
                    }).map(String::toUpperCase)
                    .forEach(s -> {
                        try {
                            Files.createDirectories(Paths.get(outputFolder));
                            Files.writeString(Paths.get(outputFolder + "/output"), s + "\n", StandardOpenOption.APPEND);
                        } catch (IOException e) {
                            throw new RuntimeException("Unable to write to:" + outputFolder);
                        }
                    });
        } catch (IOException e) {
            throw new RuntimeException("Unable to read from:" + inputFolder);
        }
    }

}

class Score {
    public Score(Student student, Subject subject, Integer score) {
        this.student = student;
        this.subject = subject;
        this.score = score;
    }

    Student student;
    Subject subject;
    Integer score;
}

enum Subject {
    SCIENCE, MATHEMATICS, LANGUAGE, HISTORY
}

class Student {
    String id;
    Map<Subject, Integer> report;

    public Student(String id, Map<Subject, Integer> report) {
        this.id = id;
        this.report = report;
    }

    @Override
    public String toString() {
        return "Student{" + "id='" + id + '}';
    }
}
