## Advanced Java Assignment

1. Given a `List<Integer>` as the input, write a function that returns the average of all elements

2. Complete the following method.

```java
/**
* Return a single List of String adding all the Strings from List of Lists given as the input.
*/
public static List<String> getFlattenList(List<List<String>> listOfListOfString){

}
```


3. Complete the following method.

```java
/**
* This method prints Each key-value pair in a new line in the following format:
* KEY: {key} = VALUE: {value}
*
* Ex:
* KEY: 1 = VALUE: JOEY
* KEY: 2 = VALUE: CHANDLER
*/
public static void printMapKeyValue(Map<String, String> map){

}
```

4. Complete the following method.

```java
/**
* Search the given for the key. If the key is found in the map, return the value. Otherwise return the String "NOT_FOUND".
*/
public static String findValue(Map<String, String> map,  String searchKey){

}
```

5. given a `List<String> `with different Strings, write a function that returns a `Map<Integer, List<String>>` where Key is integer and value is a List of Strings containing all the Strings that has the length equivalent to the key. 

6. Given below are classes representing `Student` and `Score`.

```java
class Student {
    String id;
    Map<String, Integer> report;

    public Student(String id, Map<String, Integer> report) {
        this.id = id;
        this.report = report;
    }

    @Override
    public String toString() {
        return "Student{" + "id='" + id + '}';
    }
}

enum Subject {
    SCIENCE, MATHEMATICS, LANGUAGE, HISTORY
}
```

Complete the following method. Given a list of students, this method should return a Map that contains the highest scoring students for each subject (Subject being the key).
If two students have the same highest score, the student with lowest ID has to considered as the highest scorer for the subject.

```java
public static Map<Subject, List<Student>> getBestScores(List<Student> students) {

}
```

7. You are given a folder with multiple files. Each file contains many lines each with a random String.
Write a program (a main method is fine) that reads all the lines in each of the files and convert them to uppercase. Generate a single output file containing all the lines you converted already to uppercase. Don't have to worry about the order.

Ex: Assume a folder has 2 files.

File 1:

```
Java
Spring
vertX
```

File 2:

```
Angular
REACT
typeScript
```

Output can be:

```
ANGULAR
REACT
TYPESCRIPT
JAVA
SPRING
VERTX
````

### DELIVERABLES

Create a project by the name `advanced-java-assignment`.
Please push your assignment code to GitLab. Make sure to write readable code. Each assignment should have at least a main method OR a unit test that can be used to run and evaluate the program. Make sure the question number is added to the Java comments.

You should create sample data to test the code in each of the use-case. 

### DEADLINE

Please complete the exercise before 13th of November. If you have any questions, please reach out to us.